-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 02-03-2021 a las 00:09:16
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `store`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `iva` double NOT NULL,
  `stock` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT 0,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `sku`, `name`, `description`, `image`, `price`, `iva`, `stock`, `state`, `created_date`, `deleted_date`) VALUES
(1, 'prueba1', 'prueba update', 'update prueba', 'pruebayes', 30000, 5700, 1, 0, '2021-03-01 16:39:29', '2021-03-01 21:06:08'),
(2, 'prueba2', 'prueba2', 'nada', 'hello.png', 10000, 200, 1, 0, '2021-03-01 19:30:29', NULL),
(3, 'SVC21VP4KQ4TT1325', 'prueba 3', 'imagenes', 'url', 0, 0, 1, 0, '2021-03-01 19:58:28', NULL),
(4, 'SVC21VP4KQ4TTWQK', 'prueba', 'prueba4', 'prueba4', 30000, 20, 1, 0, '2021-03-01 20:21:24', NULL),
(5, '9O9QB866A3HPF5TN', 'prueba', 'prueba5', 'prueba4', 30000, 20, 1, 0, '2021-03-01 20:25:34', NULL),
(6, 'JS7KEGTE9T11Q9T8', 'prueba', 'prueba5', 'prueba4', 30000, 5700, 1, 0, '2021-03-01 20:26:54', NULL),
(7, 'ECC58JITLL2INZW6', 'prueba', 'prueba5', 'prueba4', 30000, 5700, 1, 0, '2021-03-01 20:44:58', NULL),
(8, 'GMUSDR35X6XEQTGH', 'prueba', 'prueba5', 'prueba4', 30000, 5700, 1, 0, '2021-03-01 21:35:20', NULL),
(9, 'IDBOWJCIAYD60H34', 'prueba', 'prueba5', 'prueba4', 30000, 5700, 1, 0, '2021-03-01 22:20:45', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sale`
--

CREATE TABLE `sale` (
  `id` int(11) NOT NULL,
  `invoice_number` int(11) NOT NULL,
  `client_name` varchar(50) NOT NULL,
  `client_phone` varchar(50) NOT NULL,
  `client_email` varchar(50) NOT NULL,
  `products` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `subtotal` double NOT NULL,
  `iva` double NOT NULL,
  `total` double NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sale`
--

INSERT INTO `sale` (`id`, `invoice_number`, `client_name`, `client_phone`, `client_email`, `products`, `subtotal`, `iva`, `total`, `created_date`, `deleted_date`) VALUES
(1, 1, 'camilo', '319743132', 'sombra@correo.com', '[{     \'name\':\'prueba2\',     \'amount\':2,     \'value\':10000,     \'iva\':0.19,     \'total\':23800   },   {     \'name\':\'prueba2\',     \'amount\':2,     \'value\':10000,     \'iva\':0.19,     \'total\':23800   }]', 20000, 3800, 23800, '2021-03-01 21:55:31', '2021-03-01 23:04:08'),
(3, 2, 'leon', '31977777777', 'leon@correo.com', '[{     \'name\':\'prueba2\',     \'amount\':2,     \'value\':10000,     \'iva\':0.19,     \'total\':23800   },   {     \'name\':\'prueba2\',     \'amount\':2,     \'value\':10000,     \'iva\':0.19,     \'total\':23800   }]', 30000, 3800, 33800, '2021-03-01 22:21:20', NULL),
(4, 3, 'probando', '31911111111', 'prueba123@correo.com', '[{     \"name\":\"prueba2\",     \"amount\":2,     \"value\":10000,     \"iva\":0.19,     \"total\":23800   },   {     \"name\":\"prueba2\",     \"amount\":2,     \"value\":10000,     \"iva\":0.19,     \"total\":23800   }]', 38556, 0.19, 47600, '2021-03-01 22:59:07', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indices de la tabla `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `sale`
--
ALTER TABLE `sale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
