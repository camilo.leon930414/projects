<?php

namespace App\Http\Controllers\api;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Sale;
use App\Http\Controllers\Controller as apiController;

class SaleController extends apiController
{
    /***** lista de facturas todos o por numero de factura ****/
    function list(Request $request){
        $db = new Sale();

        try{

            if(isset($request->id)){
                $sale = $db::find($request->id);
                if($sale->deleted_date != null){// producto eliminado
                    return \Response::json(['Sale deleted'], 500);
                }
                if(empty($sale)){
                    return \Response::json(['Sale not exist'], 500);
                }
            }else{
                $sale =  $db::all()->whereNull('deleted_date');// filtrando por factura no eliminada
            }
            return response()->json(['data' => $sale]);
        
        }catch (Exception $e) {
            \Log::info('Error list facturas: '.$e);
            return \Response::json(['list error'], 500);
        }
    }
    /***** funcion para registrar facturas ****/

    function create(Request $request){
        try{
            $newSale = new Sale();
            /* valida los campos oblogatorios */
            $request->validate([
                'client_name' => 'required',
                'client_phone' => 'required',
                'client_email' => 'required',
                'products' => 'required'
            ]);
            /* se arma el objeto */
            $nextNumber = $newSale::all()->max('invoice_number');
            $newSale->invoice_number = $nextNumber+1;
            $newSale->client_name = $request->client_name;
            $newSale->client_phone = $request->client_phone;
            $newSale->client_email = $request->client_email;
            $newSale->products = $request->products;

            $calculations = $this->values($request->products);
            $newSale->subtotal = $calculations['sub'];
            $newSale->iva = $calculations['iva'];
            $newSale->total = $calculations['total'];

            if($newSale->save()){
                return \Response::json(['Sales create']);
            }else{
                return \Response::json(['Sales not created'], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error list Sales: '.$e);
            return \Response::json(['not sale'], 500);
        }
    }

    function values($products){
        $subtotal = 0;
        $iva = 0;
        $total = 0;
        $data = json_decode($products);

        foreach ($data as $key => $value) {
            $iva = $value->iva;
            $total+=$value->total;
        }
        $subtotal = $total*$iva;
        $subtotal = $total-$subtotal;

        $response = ['sub' => $subtotal, 'iva' => $iva, 'total' => $total];

        return $response;
    }
    
    /***** funcion para eliminar facturas ****/
    function delete(Request $request){
        try{
            $db = new Sale();
            $date = Carbon::now();
            $date = $date->modify('-5 hours');//hora colombia

            $sale = $db::find($request->id);
            $sale ->deleted_date = $date->format('Y-m-d H:i:s');

            if($sale->save()){
                return \Response::json(['product deleted']);
            }else{
                return \Response::json(['product not deleted'], 500);
            }
 
        }catch (Exception $e) {
            \Log::info('Error delete product: '.$e);
            return \Response::json(['not deleted'], 500);
        }
    }

}
