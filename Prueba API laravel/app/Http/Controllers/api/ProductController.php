<?php

namespace App\Http\Controllers\api;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Controllers\Controller as apiController;

class ProductController extends apiController
{
   /***** lista de productos todos o por id ****/
    function list(Request $request){
        $db = new Product();

        try{

            if(isset($request->id)){
                $products = $db::find($request->id);
                if($products->state != 0){//estado activado = 0
                    return \Response::json(['Product not disabled'], 500);
                }
                if($products->deleted_date != null){// producto eliminado
                    return \Response::json(['Product deleted'], 500);
                }
                if(empty($products)){
                    return \Response::json(['Product not exist'], 500);
                }
            }else{
                $products =  $db::all()->where('state','=',0)->whereNull('deleted_date');// filtrando estado activado = 0 y producto no eliminado
            }
            
            return response()->json(['data' => $products]);
        
        }catch (Exception $e) {
            \Log::info('Error list products: '.$e);
            return \Response::json(['list error'], 500);
        }
    }
    /***** funcion para registrar productos ****/

    function create(Request $request){
        try{
            $newProduct = new Product();
            /* valida los campos oblogatorios */
            $request->validate([
                'name' => 'required',
                'description' => 'required',
                'image' => 'required',
                'price' => 'required',
                'stock' => 'required',
            ]);
            /* se arma el objeto */
            $sku = $this->createSku();
            $iva = 0.19;//iva del 19%
            $newProduct->sku = $sku;
            $newProduct->name = $request->name;
            $newProduct->description = $request->description;
            $newProduct->image = $request->image;
            $newProduct->price = $request->price;
            $newProduct->iva = $request->price*$iva;
            $newProduct->stock = $request->stock;

            if($newProduct->save()){
                return \Response::json(['Product create']);
            }else{
                return \Response::json(['Product not created'], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error list products: '.$e);
            return \Response::json(['not list'], 500);
        }
    }
    /* crear sku */
    function createSku(){
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $input_length = strlen($permitted_chars);
        $SkuCreated = '';
        for($i = 0; $i < 16; $i++) {
            $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
            $SkuCreated.= $random_character;
        }
        return strtoupper($SkuCreated);
    }
    /***** funcion para registrar actualizar productos ****/

    function update(Request $request){
        try{
            $db = new Product();
            /* valida los campos oblogatorios */
            $product = $db::find($request->id);
            $iva = 0.19;//iva del 19%
            $request->validate([
                'id' => 'required'
            ]);
            /* se arma el objeto */
            $product->name = isset($request->name)?$request->name:$product->name;
            $product->description = isset($request->description)?$request->description:$product->description;
            $product->image = isset($request->image)?$request->image:$product->image;
            $product->price = isset($request->price)?$request->price:$product->price;
            $product->iva = isset($request->price)?$request->price*$iva:$product->iva;
            $product->state = isset($request->state)?$request->state:$product->state;
            $product->stock = isset($request->stock)?$request->stock:$product->stock;
            if($product->save()){
                return \Response::json(['product update']);
            }else{
                return \Response::json(['product not updated' => false], 500);
            }
        }catch (Exception $e) {
            \Log::info('Error update products: '.$e);
            return \Response::json(['not update'], 500);
        }
    }
    /***** funcion para eliminar productos ****/
    function delete(Request $request){
        try{
            $db = new Product();
            $date = Carbon::now();
            $date = $date->modify('-5 hours');//hora colombia

            $product = $db::find($request->id);
            $product ->deleted_date = $date->format('Y-m-d H:i:s');

            if($product->save()){
                return \Response::json(['product deleted']);
            }else{
                return \Response::json(['product not deleted'], 500);
            }
 
        }catch (Exception $e) {
            \Log::info('Error delete product: '.$e);
            return \Response::json(['not deleted'], 500);
        }
    }
}
