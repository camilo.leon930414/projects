<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\ProductController;
use App\Http\Controllers\api\SaleController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/* productos */
/* lista de productos v1/list/products y tambien por id*/
Route::get('v1/list/products/{id?}',[ProductController::class, 'list']);
/* creacion de productos */
Route::post('v1/create/product',[ProductController::class, 'create']);
/* actualizar productos */
Route::post('v1/update/product',[ProductController::class, 'update']);
/* eliminar producto */
Route::get('v1/delete/product/{id}',[ProductController::class, 'delete']);

/* venta */
/* lista de facturas v1/list/invoice y tambien por id*/
Route::get('v1/list/invoice/{id?}',[SaleController::class, 'list']);
/* creacion de facturas */
Route::post('v1/create/invoice',[SaleController::class, 'create']);
/* eliminar producto */
Route::get('v1/delete/invoice/{id}',[SaleController::class, 'delete']);
