<?php
declare(strict_types=1);

 

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;


class VentasController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        //
    }

    /**
     * Searches for ventas
     */
    public function searchAction()
    {
        $numberPage = $this->request->getQuery('page', 'int', 1);
        $parameters = Criteria::fromInput($this->di, 'Ventas', $_GET)->getParams();
        $parameters['order'] = "id";

        $paginator   = new Model(
            [
                'model'      => 'Ventas',
                'parameters' => $parameters,
                'limit'      => 10,
                'page'       => $numberPage,
            ]
        );

        $paginate = $paginator->paginate();

        if (0 === $paginate->getTotalItems()) {
            $this->flash->notice("The search did not find any ventas");

            $this->dispatcher->forward([
                "controller" => "ventas",
                "action" => "index"
            ]);

            return;
        }

        $this->view->page = $paginate;
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        //
    }

    /**
     * Edits a venta
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {
            $venta = Ventas::findFirstByid($id);
            if (!$venta) {
                $this->flash->error("venta was not found");

                $this->dispatcher->forward([
                    'controller' => "ventas",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $venta->id;

            $this->tag->setDefault("id", $venta->id);
            $this->tag->setDefault("n_factura", $venta->n_factura);
            $this->tag->setDefault("direccion", $venta->direccion);
            $this->tag->setDefault("total", $venta->total);
            $this->tag->setDefault("created_at", $venta->created_at);
            $this->tag->setDefault("deleted_at", $venta->deleted_at);
            
        }
    }

    /**
     * Creates a new venta
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "ventas",
                'action' => 'index'
            ]);

            return;
        }

        $venta = new Ventas();
        $venta->nFactura = $this->request->getPost("n_factura", "int");
        $venta->direccion = $this->request->getPost("direccion", "string");
        $venta->total = $this->request->getPost("total");
        $venta->createdAt = $this->request->getPost("created_at", "string");
        $venta->deletedAt = $this->request->getPost("deleted_at", "string");
       
        if (!$venta->save()) {
            foreach ($venta->getMessages() as $message) {
                var_dump($message);die;
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "ventas",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("venta was created successfully");

        $this->dispatcher->forward([
            'controller' => "ventas",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a venta edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "ventas",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $venta = Ventas::findFirstByid($id);

        if (!$venta) {
            $this->flash->error("venta does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "ventas",
                'action' => 'index'
            ]);

            return;
        }

        $venta->nFactura = $this->request->getPost("n_factura", "int");
        $venta->direccion = $this->request->getPost("direccion", "int");
        $venta->total = $this->request->getPost("total", "int");
        $venta->createdAt = $this->request->getPost("created_at", "int");
        $venta->deletedAt = $this->request->getPost("deleted_at", "int");
        

        if (!$venta->save()) {

            foreach ($venta->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "ventas",
                'action' => 'edit',
                'params' => [$venta->id]
            ]);

            return;
        }

        $this->flash->success("venta was updated successfully");

        $this->dispatcher->forward([
            'controller' => "ventas",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a venta
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $venta = Ventas::findFirstByid($id);
        if (!$venta) {
            $this->flash->error("venta was not found");

            $this->dispatcher->forward([
                'controller' => "ventas",
                'action' => 'index'
            ]);

            return;
        }

        if (!$venta->delete()) {

            foreach ($venta->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "ventas",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("venta was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "ventas",
            'action' => "index"
        ]);
    }
}
