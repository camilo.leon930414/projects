<?php
declare(strict_types=1);

 

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;


class VendedoresController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        //
    }

    /**
     * Searches for vendedores
     */
    public function searchAction()
    {
        $numberPage = $this->request->getQuery('page', 'int', 1);
       /*  $parameters = Criteria::fromInput($this->di, 'Vendedores', $_GET)->getParams();
        $parameters['order'] = "id"; */

        $paginator   = new Model(
            [
                'model'      => 'Vendedores',
                /* 'parameters' => $parameters, */
                'limit'      => 10,
                'page'       => $numberPage,
            ]
        );

        $paginate = $paginator->paginate();

        if (0 === $paginate->getTotalItems()) {
            $this->flash->notice("The search did not find any vendedores");

            $this->dispatcher->forward([
                "controller" => "vendedores",
                "action" => "index"
            ]);

            return;
        }

        $this->view->page = $paginate;
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        //
    }

    /**
     * Edits a vendedore
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {
            $vendedore = Vendedores::findFirstByid($id);
            if (!$vendedore) {
                $this->flash->error("vendedore was not found");

                $this->dispatcher->forward([
                    'controller' => "vendedores",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $vendedore->id;

            $this->tag->setDefault("id", $vendedore->id);
            $this->tag->setDefault("nombre", $vendedore->nombre);
            $this->tag->setDefault("identificacion", $vendedore->identificacion);
            $this->tag->setDefault("created_at", $vendedore->created_at);
            $this->tag->setDefault("deleted_at", $vendedore->deleted_at);
            
        }
    }

    /**
     * Creates a new vendedore senecesitan los datos de formulario para servir
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "vendedores",
                'action' => 'index'
            ]);

            return;
        }

        $vendedore = new Vendedores();
        $vendedore->nombre = $this->request->getPost("nombre", "int");
        $vendedore->identificacion = $this->request->getPost("identificacion", "int");
        $vendedore->createdAt = $this->request->getPost("created_at", "int");
        $vendedore->deletedAt = $this->request->getPost("deleted_at", "int");
        

        if (!$vendedore->save()) {
            foreach ($vendedore->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "vendedores",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("vendedore was created successfully");

        $this->dispatcher->forward([
            'controller' => "vendedores",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a vendedore edited
     *
     */
    public function saveAction()
    {
        
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "vendedores",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        
        $vendedore = Vendedores::findFirstByid($id);

        if (!$vendedore) {
            $this->flash->error("vendedore does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "vendedores",
                'action' => 'index'
            ]);

            return;
        }

        $vendedore->nombre = $this->request->getPost("nombre", "int");
        $vendedore->identificacion = $this->request->getPost("identificacion", "string");
        $vendedore->createdAt = $this->request->getPost("created_at", "int");
        $vendedore->deletedAt = $this->request->getPost("deleted_at", "int");
        

        if (!$vendedore->save()) {

            foreach ($vendedore->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "vendedores",
                'action' => 'edit',
                'params' => [$vendedore->id]
            ]);

            return;
        }else
         /* logica */
        
        $this->flash->success("vendedore was updated successfully");

        $this->dispatcher->forward([
            'controller' => "vendedores",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a vendedore
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $vendedore = Vendedores::findFirstByid($id);
        if (!$vendedore) {
            $this->flash->error("vendedore was not found");

            $this->dispatcher->forward([
                'controller' => "vendedores",
                'action' => 'index'
            ]);

            return;
        }

        if (!$vendedore->delete()) {

            foreach ($vendedore->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "vendedores",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("vendedore was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "vendedores",
            'action' => "index"
        ]);
    }
}
