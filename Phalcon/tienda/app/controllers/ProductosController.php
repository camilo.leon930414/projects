<?php
declare(strict_types=1);

 

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;


class ProductosController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        //
    }

    /**
     * Searches for productos
     */
    public function searchAction()
    {
        $numberPage = $this->request->getQuery('page', 'int', 1);
        $parameters = Criteria::fromInput($this->di, 'Productos', $_GET)->getParams();
        $parameters['order'] = "id";

        $paginator   = new Model(
            [
                'model'      => 'Productos',
                /* 'parameters' => $parameters, */
                'limit'      => 5,
                'page'       => $numberPage,
            ]
        );

        $paginate = $paginator->paginate();

        if (0 === $paginate->getTotalItems()) {
            $this->flash->notice("The search did not find any productos");

            $this->dispatcher->forward([
                "controller" => "productos",
                "action" => "index"
            ]);

            return;
        }

        $this->view->page = $paginate;
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        //
    }

    /**
     * Edits a producto
     *
     * @param string $id
     */
    public function editAction($id)
    {   
        if (!$this->request->isPost()) {
            $producto = Productos::findFirstByid($id);
            if (!$producto) {
                $this->flash->error("producto was not found");

                $this->dispatcher->forward([
                    'controller' => "productos",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $producto->id;

            $this->tag->setDefault("id", $producto->id);
            $this->tag->setDefault("nombre", $producto->nombre);
            $this->tag->setDefault("cantidad", $producto->cantidad);
            $this->tag->setDefault("precio", $producto->precio);
            $this->tag->setDefault("created_at", $producto->created_at);
            $this->tag->setDefault("deleted_at", $producto->deleted_at);
            
        }
    }

    /**
     * Creates a new producto
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "productos",
                'action' => 'index'
            ]);

            return;
        }

        $producto = new Productos();

       /*  $producto->imagen =(base64_encode (file_get_contents ($this->request->getUploadedFiles()[0]->getTempName())));
        print_r($imagen);die; */
        $producto->nombre = $_POST['nombre'];
        $producto->cantidad = $_POST['cantidad'] ;
        $producto->precio = $_POST['precio'];
           
        $nombreFile = $_FILES['imagen']['name'];

        /*  $url = "public/img/prueba/$nombreFile"; */
        /* guardado files */
        if($_FILES['imagen']['error'] == 0){
            $imagenB64 = base64_encode(file_get_contents($_FILES['imagen']['tmp_name']));
            $producto->imagen = $imagenB64;
        }
        
        if (!$producto->save()) {
            foreach ($producto->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "productos",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("producto was created successfully");

        $this->dispatcher->forward([
            'controller' => "productos",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a producto edited
     *
     */
    public function saveAction()
    {
        
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "productos",
                'action' => 'index'
            ]);

            return;
        }
        $id = $this->request->getPost("id");

        $producto = Productos::findFirstByid($id);
        if (!$producto) {
            $this->flash->error("producto does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "productos",
                'action' => 'index'
            ]);

            return;
        }

        $producto->nombre = $this->request->getPost("nombre", "string");
        $producto->cantidad = $this->request->getPost("cantidad", "int");
        $producto->precio = $this->request->getPost("precio", "int");
        $producto->createdAt = $this->request->getPost("created_at", "string");
        $producto->deletedAt = $this->request->getPost("deleted_at", "string");
        

        if (!$producto->save()) {

            foreach ($producto->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "productos",
                'action' => 'edit',
                'params' => [$producto->id]
            ]);

            return;
        }

        $this->flash->success("producto was updated successfully");

        $this->dispatcher->forward([
            'controller' => "productos",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a producto
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $producto = Productos::findFirstByid($id);
        if (!$producto) {
            $this->flash->error("producto was not found");

            $this->dispatcher->forward([
                'controller' => "productos",
                'action' => 'index'
            ]);

            return;
        }

        if (!$producto->delete()) {

            foreach ($producto->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "productos",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("producto was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "productos",
            'action' => "index"
        ]);
    }
}
